<?php


namespace App;


class Zoo
{
    private $zoo;
    public function __construct()
    {
        $this->zoo=array(
            new Animal('Слон','травоїдний'),
            new Animal('Вовк','хижак'),
            new Animal('Рись','хижак'),

        );
    }
    public function Search($name, $query){
        return ($name==$query);
    }
    public function Output($type){
        $i=0;
        foreach ($this->zoo as $value){
            if($value->getType()==$type)
                $i++;
        }
        return $i;
    }

}